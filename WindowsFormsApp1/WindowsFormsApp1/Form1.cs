﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = Properties.Settings.Default.Form1State;
            this.Location = Properties.Settings.Default.Form1Point;
            this.Size = Properties.Settings.Default.Form1Size;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Form1State = this.WindowState;
            Properties.Settings.Default.Form1Size = this.Size;
            Properties.Settings.Default.Form1Point = this.Location;
        }
    }
}
